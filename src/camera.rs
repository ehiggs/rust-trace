use vecmath::{Vector4, Matrix4};
use image::Rgb;
use rand::{thread_rng, ThreadRng};
use rand::distributions::{IndependentSample, Range};
use diaphram::{Diaphram, diacircle};
use types::{Ray, Vector};

pub struct Context {
    rng: ThreadRng
}

pub struct Camera {
	focal_len: f32,
	focus: f32,
	aperture: f32,
	diaphragm: Box<Diaphram>,
	transf:    Matrix4<f32>,
	anti_alias: bool
}

impl Camera {
    pub fn ray_from(&self, context: &mut Context, i: u32, j: u32, width: u32, height: u32) -> Ray {
        let mut r = Ray{start: Vector::new(0., 0., 0.), direction: Vector::new(0.,0.,0.)};
        if self.aperture > 0. {
            let (mut xs, mut ys) = (*self.diaphragm)(&mut context.rng);
            xs *= self.aperture;
            ys *= self.aperture;
            r.start = Vector::new(xs as f32, ys as f32, 0.);
        }
		// ray end point
		let y0 = (-(i as f32) + self.aa(&mut context.rng) + (height as f32)/2.) / (height as f32);
		let x0 = (((j as f32) + self.aa(&mut context.rng) - (width as f32)/2.) / (height as f32));
		let z0 = self.focal_len;

		let mut end = Vector::new(x0 as f32, y0 as f32, z0 as f32);
		if self.focus > 0. {
			end = end.mul(self.focus as f32);
		}

		// ray direction
		if self.focal_len != 0. {
			let start = r.start;
			r.set_dir((end - start).normalize());
		} else {
			r.set_dir(Vector::new(0., 0., 1.));
			r.start = Vector::new(x0 as f32, y0 as f32, 0.);
		}

		// camera transform
		r.transf(self.transf);
		r
    }

	/*
	// https://github.com/barnex/bruteray/blob/master/raster/camera.go#L74
	fn transform(&self, T: &Matrix4<f32>) -> &Self {
		self.transf = *((&self.transf).mul(T));
		self;
	}
	*/

	// Anti-aliasing jitter
	fn aa(&self, mut rng: &mut ThreadRng) -> f32 {
		if self.anti_alias {
            let between = Range::new(0f32, 1f32);
			return between.ind_sample(&mut rng);
		} else {
			return 0.5;
		}
	}
}
