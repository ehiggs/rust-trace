use image::Rgb;

pub fn from_color(rgb: &Rgb<u8>) -> (u8, u8, u8) {
    (rgb[0], rgb[1], rgb[2])
}

pub fn to_color(r: f32, g: f32, b: f32) -> Rgb<u8> {
    let f = |x: f32| { (srgb(x as f32 / 255.0) * 255.0).round() as u8 };
    Rgb::<u8> { data: [ f(r), f(g), f(b)] }
}

/**
 * Convert an rgb value into an srgb value.
 */
pub fn srgb(x: f32) -> f32 {
    if x <= 0.0031308 {
        12.92 * x 
    } else {
        1.055*x.powf(1.0 / 2.4) - 0.05 
    }
}
