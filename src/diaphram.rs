use rand::ThreadRng;
use rand::distributions::{IndependentSample, Range};

pub type Diaphram = Fn(&mut ThreadRng) -> (f32, f32);

pub fn diacircle(mut rng: &mut ThreadRng) -> (f32, f32) {
    let between = Range::new(0f32, 1f32);
    let mut x = 2. * between.ind_sample(&mut rng) - 1.;
    let mut y = 2. * between.ind_sample(&mut rng) - 1.;
    while (x*x + y*y) > 1. {
        x = 2. * between.ind_sample(&mut rng) - 1.;
        y = 2. * between.ind_sample(&mut rng) - 1.;
    }
    (x, y)
}
