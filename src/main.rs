extern crate image;
extern crate rand;
extern crate vecmath;

mod diaphram;
mod camera;
mod color;
mod material;
mod types;
mod sphere;

use types::{Env, EnvBuilder, Ray, Vector};
use sphere::Sphere;

const WIDTH: u32 = 800;
const HEIGHT: u32 = 600;

fn main() {
    let white = color::to_color(1.0, 1.0, 1.0);
    let c = Vector::new(0., 0., -2.);
    let flat_white = Box::new(material::Flat{color: white});
    let env = EnvBuilder::new()
        .set_bg_color(white)
        .add_object(Sphere{center: c, radius: 0.5, material: flat_white})
        .add_light()
        .set_ambient_light()
        .set_camera(Vector::new(0., 0., -1.))
        .build();
    env.serve("output.png", WIDTH, HEIGHT);
}
