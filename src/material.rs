use types::{Env, Ray, Fragment};
use image::Rgb;

pub trait Material {
    fn shade(&self, env: &Env, recursion: i32, ray: &Ray, frag: &Fragment) -> Rgb<u8>;
}

pub struct Flat {
    pub color: Rgb<u8>
}

impl Material for Flat {
    fn shade(&self, env: &Env, recursion: i32, ray: &Ray, frag: &Fragment) -> Rgb<u8> {
        self.color
    }
}

/*
pub struct Diffuse {
    refl: Rgb<u8>
}

impl Material for Diffuse {
    fn shade(&self, env: &Env, recursion: i32, ray: &Ray, frag: &Fragment) -> Rgb<u8> {
        let pos== ray.at(frag.t-offset);
        let norm = frag.norm;
        let mut acc : Rgb<u8>;

        // first sum over all explicit sources
        // (with fall-off)
        for _, l := range e.Lights {
            acc = acc.Add(s.lightIntensity(e, pos, norm, l));
        }

        // add one random ray to sample (only!) the indirect sources.
        // (no fall-off, the chance of hitting an object
        // automatically falls off correctly with distance).
        // Choose the random ray via importance sampling.
        sec = e.NewRay(pos.MAdd(offset, norm), RandVecCos(e, norm));
        defer e.RRay(sec);
        acc = acc.Add(s.refl.Mul3(e.ShadeNonLum(ctx, sec, N))); // does not include explicit lights

        acc
    }
}
*/
