use types::{Env, Object, Ray, Vector, Fragment};
use material::Material;

/**
 * A shape is actually a step in the render pipeline that takes the Env and 
 * Ray and returns the distance and a UnitVector of the surface normal.
 */
pub struct Sphere {
    pub center: Vector,
    pub radius: f32,
    pub material: Box<Material>
}

impl Object for Sphere {
    fn hit(&self, ray: &Ray) -> Option<Fragment> {
        let v = ray.start - self.center;
        let discr = v.dot(ray.direction).powi(2) - v.dot(v) +  self.radius.powi(2);
        let dist = -v.dot(ray.direction) - discr.sqrt();
        let point = ray.at(dist);
        let normal = (point - self.center).normalize();
        if discr < 0f32 {
            None
        } else {
            None
            //Some((dist, normal))
        }
    }

    fn inside(&self, p: Vector) -> bool {
        let v = p - self.center;
        v.length() < self.radius
    }
}
