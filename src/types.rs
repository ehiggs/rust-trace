use std::ops;
use image::{ImageBuffer, Rgb};

use color::{from_color, srgb, to_color};
use material::Material;
use vecmath::{Vector3, Matrix4, Vector4, vec4_dot};

/**
 * A Fragment is an infinitesimally small surface element.
 */
pub struct Fragment {
    t: f32,
    norm: Vector,
    material: Box<Material>,
    o: Box<Object>,
}

impl Fragment {
    // why is this mut? seems stupid.
    pub fn shade(&mut self, env: &Env, recursion: i32, ray: &Ray) -> Rgb<u8> {
        self.norm = self.norm.towards(ray.direction).normalize();
        self.material.shade(env, recursion, ray, self)
    }
}

/**
 * The result of a pipeline is an Object: a distance and a color.
 */
pub trait Object {
    fn hit(&self, ray: &Ray) -> Option<Fragment>;
    fn inside(&self, p: Vector) -> bool;
}

pub struct EnvBuilder {
    scene: Vec<Box<Object>>,
    bg_color: Option<Rgb<u8>>,
    camera: Option<Vector>,
}

impl EnvBuilder {
    pub fn new() -> EnvBuilder {
        EnvBuilder {
            scene: Vec::new(),
            bg_color: None,
            camera: None
        }
    }

    pub fn add_object<O: Object + 'static>(mut self, obj: O) -> Self {
        self.scene.push(Box::new(obj));
        self
    }

    // TODO
    pub fn add_light(mut self) -> Self {
        self
    }

    pub fn set_bg_color(mut self, color: Rgb<u8>) -> Self {
        self.bg_color = Some(color);
        self
    }

    // TODO
    pub fn set_ambient_light(mut self) -> Self {
        self
    }

    pub fn set_camera(mut self, camera: Vector) -> Self {
        self.camera = Some(camera);
        self
    }

    pub fn build(self) -> Env {
        Env {
            scene: self.scene,
            bg_color: self.bg_color.unwrap(),
            camera: self.camera.unwrap()
        }
    }
}

pub struct Env {
    scene: Vec<Box<Object>>,
    bg_color: Rgb<u8>,
    camera: Vector
}

impl Env {
    fn render(&self, i: u32, j: u32, width: u32, height: u32) -> Rgb<u8> {
        let (x, y) = Env::pixel_to_coordinate(i, j, width, height);
        let ray = Ray::from_floats(x, y);
        let (r, g, b) = from_color(&find_color(self, ray));
        if (i % 100) == 0  && (j % 100) == 0 {
            println!("x: {} y: {} ray: {:?} color: {:?}", i, j, ray, (r,g,b));
        }
        Rgb::<u8> { data: [ r, g, b ] }
    }

    fn pixel_to_coordinate(i: u32, j: u32, width: u32, height: u32) -> (f32, f32) {
        let a = (width as f32)/(height as f32);
        let x = a*(2.0*(i as f32 + 0.5)/width as f32 - 1.0);
        let y = -2.0 * (j as f32 + 0.5) / height as f32 + 1.0;
        (x, y)
    }

    pub fn serve(&self, output_path: &str, width: u32, height: u32) {
        let mut image = ImageBuffer::<Rgb<u8>, Vec<u8>>::new(width, height);
        for x in 0..width { 
            for y in 0..height {
                image.put_pixel(x, y, self.render(x, y, width, height));
            }
        }
        // write it out to a file
        image.save(output_path).unwrap();
    }
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub struct Vector {
    x: f32,
    y: f32,
    z: f32
}

impl Vector {
    pub fn new(x: f32, y: f32, z:f32) -> Vector {
        Vector{x, y, z}
    }

    pub fn from_float(f: f32) -> Vector {
        Vector{x: f, y: f, z: f}
    }

    pub fn dot(&self, other: Vector) -> f32 {
        self.x*other.x + self.y*other.y + self.z*other.z
    }

    pub fn normalize(&self) -> Vector {
        Vector::new(self.x/3., self.y/3., self.z/3.)
    }

    pub fn length(&self) -> f32 {
        let a = self.dot(*self);
        a.sqrt()
    }

    pub fn add(&self, coef: f32) -> Vector {
        Vector {
            x: self.x + coef,
            y: self.y + coef,
            z: self.z + coef,
        }
    }

    pub fn mul(&self, coef: f32) -> Vector {
        Vector {
            x: self.x * coef,
            y: self.y * coef,
            z: self.z * coef,
        }
    }

    pub fn sub(&self, coef: f32) -> Vector {
        Vector {
            x: self.x - coef,
            y: self.y - coef,
            z: self.z - coef,
        }
    }

    // May invert v to assure it points towards direction d.
    // Used to ensure normal vectors point outwards.
    pub fn towards(self, d:  Vector) -> Vector {
        if self.dot(d) > 0. {
            self.mul(-1.)
        } else {
            self
        }
    }
}

impl ops::Add for Vector {
    type Output = Vector;
    fn add(self, other: Vector) -> Vector {
        Vector {
            x: self.x + other.x,
            y: self.y + other.y,
            z: self.z + other.z,
        }
    }
}
impl ops::Sub for Vector {
    type Output = Vector;
    fn sub(self, other: Vector) -> Vector {
        Vector {
            x: self.x - other.x,
            y: self.y - other.y,
            z: self.z - other.z,
        }
    }
}

impl ops::Mul for Vector {
    type Output = Vector;
    fn mul(self, coef: Vector) -> Vector {
        Vector {
            x: self.x * coef.x,
            y: self.y * coef.y,
            z: self.z * coef.z,
        }
    }
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub struct Ray {
    pub start: Vector,
    pub direction: Vector
}

impl Ray {
    pub fn from_floats(x: f32, y: f32) -> Ray {
        let start = Vector::new(x, y, 0.);
        let f = Vector::new(0., 0., 2.);
        //let dir = normalize(start - f);
        let dir = (start - f).normalize();
        Ray{start: start, direction: dir}
    }

    pub fn at(&self, t: f32) -> Vector {
        let scaled_vec = Vector::new(self.direction.x*t, self.direction.y*t, self.direction.z*t);
        self.start + scaled_vec
    }

    pub fn set_dir(&mut self, dir: Vector) {
        self.direction = dir;
    }

    pub fn transf(&mut self, t: Matrix4<f32>) {
        self.start = transf_point(self.start, t);
        let direction = self.direction;
        self.set_dir(transf_dir(direction, t));
    }
}

pub fn transf_dir(v: Vector, t: Matrix4<f32>) -> Vector {
    let r = [v.x, v.y, v.z, 0.];
    // TODO - check that we are column based; otherwise use row_mat4_col(...)
    Vector::new(vec4_dot(r, t[0] as [f32;4]),
                vec4_dot(r, t[1] as [f32;4]),
                vec4_dot(r, t[2] as [f32;4]))
}

pub fn transf_point(v: Vector, t: Matrix4<f32>) -> Vector {
    let r = [v.x, v.y, v.z, 1.];
    // TODO - check that we are column based; otherwise use row_mat4_col(...)
    Vector::new(vec4_dot(r, t[0] as [f32;4]),
                vec4_dot(r, t[1] as [f32;4]),
                vec4_dot(r, t[2] as [f32;4]))

}

pub fn find_color(environ: &Env, r: Ray) -> Rgb<u8> {
    /*
    let shapes = scene(environ);
    let (f, shape) = shape(environ, ray);
    let distAndColors = f.map(shapes);
    let sortedTs 
    */
    Rgb::<u8> { data: [255, 255, 255] }
}


